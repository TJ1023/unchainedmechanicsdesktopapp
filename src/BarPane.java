import javafx.scene.layout.BorderPane;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import javafx.scene.control.Label;

public class BarPane extends BorderPane{
	
	DataImporter data = new DataImporter();
	
	//Initialize age areas
	final static String Below18 = "below 18";
	final static String F18to30 = "18-30";
	final static String F31to50 = "31-50";
	final static String F51to60 = "51-60";
	final static String Above60 = "above 60";

	public BarPane() {
		
		//counter for people to age
		int below18=0;
		int f18to30=0;
		int f31to50=0;
		int f51to60=0;
		int above60=0;
		
		int age;
		
		//go through data and categorize customers with age
		for(int i = 0; i < data.getData().getData_points().length; i++) {
			
			age = data.getData().getData_points()[i].getAge();
			
			if(age <= 18)
				below18++;
			
			if(age >= 18 && age <= 30)
				f18to30++;
			
			if(age >= 31 && age <= 50)
				f31to50++;
			
			if(age >= 51 && age <= 60)
				f51to60++;
	
			if(age >= 60)
				above60++;
		}
		
		//title the chart
		Label text = new Label("Bar Chart");
		text.setAlignment(Pos.CENTER);
		text.setMaxSize(100, 50);
		//setCenter(text);
		setTop(text);
		setAlignment(text, Pos.CENTER);
		
		//set up x and y axis
		final CategoryAxis xAxis = new CategoryAxis();
	    final NumberAxis yAxis = new NumberAxis();
	    final BarChart<String,Number> bc = 
	    		new BarChart<String,Number>(xAxis,yAxis);
	     
	    bc.setTitle("Age Data");
	    xAxis.setLabel("Age");       
	    yAxis.setLabel("Number of Customers");
	     
	    //inset data to the chart 
	    XYChart.Series series1 = new XYChart.Series();
	        
	    series1.getData().add(new XYChart.Data(Below18, below18));   
	    series1.getData().add(new XYChart.Data(F18to30, f18to30));  
	    series1.getData().add(new XYChart.Data(F31to50, f31to50));  
	    series1.getData().add(new XYChart.Data(F51to60, f51to60));  
	    series1.getData().add(new XYChart.Data(Above60, above60));  
	     
	    bc.getData().addAll(series1);
	     
	    getStylesheets().addAll(this.getClass().getResource("style.css")
	    		.toExternalForm());
	 	setId("graphButtons");
	    
	    setCenter(bc);
	}

}
