import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.image.ImageView;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.stage.*;

class GraphScene extends Scene{
	
	Insets btnMargs = new Insets(5, 5, 5, 5);
	Insets btnMargs2 = new Insets(4, 5, 4, 0);
	Insets btnMargs3 = new Insets(5, 5, 50, 5);
	Insets otherMargs = new Insets(5, 5, 350, 0);
	Insets logMargs = new Insets(5, 5, 200, 0);
	Insets fieldMargs = new Insets(0, 50, 0, 50);
	
	public GraphScene(BorderPane root, double width, double height) {
		super(root, width, height);
		
	GridPane graphPane = new GridPane();
	root.setCenter(graphPane);
	root.setId("graphScene");
//menu and component buttons
	HBox menu = new HBox();
	Button home = new Button();
	Button logOut = new Button();
	Button refresh = new Button();
	
//menu styling	
	menu.setId("menuBar");
	logOut.setId("buttons");
	home.setId("buttons");
	refresh.setId("buttons");
	
	ImageView reloadIcon = new ImageView("Reload-icon.png");
	reloadIcon.setFitHeight(16);
	home.setGraphic(new ImageView("newButton.png"));
	refresh.setGraphic(reloadIcon);
	logOut .setGraphic(new ImageView("homebuttonNew.png"));
	
//adding components to 'menu'
	menu.getChildren().add(home);
	menu.getChildren().add(refresh);
	menu.getChildren().add(logOut);
	
//adding the menu to 'root'	
	root.setTop(menu);
	
//setting margins for menu components
	menu.setMargin(home, btnMargs2);
	menu.setMargin(logOut, btnMargs2);
	menu.setMargin(refresh, btnMargs2);
	menu.setAlignment(Pos.BASELINE_RIGHT);
	
//sets the minimum width of the buttons, for uniform button lengths
	home.setMaxHeight(20);
	home.setVisible(false);
	logOut.setMaxHeight(20);
	refresh.setMaxHeight(10);
	
//creating the graph objects	
	PiePane pieGraph = new PiePane();
	LinePane lineGraph = new LinePane();
	BarPane barGraph = new BarPane();
	BubblePane bubbleGraph = new BubblePane();
	BorderPane blankPane = new BorderPane();
	
/*---------button actions--------------*/
//refresh button commands
	refresh.setOnMousePressed(e ->{
		refresh.setId("buttonsPressed");
	});
	refresh.setOnMouseReleased(e ->{
		refresh.setId("buttons");
		GUI.window.setScene(new GraphScene(new BorderPane(),
				GUI.windowWidth, GUI.windowHeight));
	});
	
//home button
	home.setOnMousePressed(e ->{
		home.setId("buttonsPressed");
	});
	home.setOnMouseReleased(e ->{
		home.setId("buttons");
		root.setCenter(graphPane);
		home.setVisible(false);
		
//logout button (new name TBD)
	});
	logOut.setOnMousePressed(e ->{
		logOut.setId("buttonsPressed");
	});
	logOut.setOnMouseReleased(e -> {
		logOut.setId("buttons");
		root.setCenter(graphPane);
		home.setVisible(false);
		GUI.window.setScene(GUI.statScene);
	});
	
//pie actions
	pieGraph.setOnMousePressed(e ->{
		pieGraph.setId("graphClick");
	});
	pieGraph.setOnMouseReleased(e -> {
		PiePane newGraph = new PiePane();
		blankPane.setCenter(newGraph);
		blankPane.setMargin(newGraph, btnMargs);
		root.setCenter(blankPane);
		pieGraph.setId("graphButtons");
		home.setVisible(true);
	});
	
//bar actions
	barGraph.setOnMousePressed(e ->{
		barGraph.setId("graphClick");
	});
	barGraph.setOnMouseReleased(e -> {
		BarPane newGraph = new BarPane();
		blankPane.setCenter(newGraph);
		blankPane.setMargin(newGraph, btnMargs);
		root.setCenter(blankPane);
		barGraph.setId("graphButtons");
		home.setVisible(true);
	});
	
//line actions
	lineGraph.setOnMousePressed(e ->{
		lineGraph.setId("graphClick");
	});
	lineGraph.setOnMouseReleased(e -> {
		LinePane newGraph = new LinePane();
		blankPane.setCenter(newGraph);
		blankPane.setMargin(newGraph, btnMargs);
		root.setCenter(blankPane);
		lineGraph.setId("graphButtons");
		home.setVisible(true);
	});
	
//bubble actions
	bubbleGraph.setOnMousePressed(e ->{
		bubbleGraph.setId("graphClick");
	});
	bubbleGraph.setOnMouseReleased(e -> {
		BubblePane newGraph = new BubblePane();
		blankPane.setCenter(newGraph);
		blankPane.setMargin(newGraph, btnMargs);
		root.setCenter(blankPane);
		bubbleGraph.setId("graphButtons");
		home.setVisible(true);
	});
	
//Adding all components to the graphPane	
	graphPane.getChildren().addAll(pieGraph, barGraph, lineGraph, bubbleGraph);
	graphPane.setConstraints(barGraph, 0, 0);
	graphPane.setConstraints(lineGraph, 1, 0);
	graphPane.setConstraints(pieGraph, 0, 1);
	graphPane.setConstraints(bubbleGraph, 1,1);
//setting component margins
	
	graphPane.setMargin(barGraph, btnMargs);
	graphPane.setMargin(pieGraph, btnMargs);
	graphPane.setMargin(lineGraph, btnMargs);
	graphPane.setMargin(bubbleGraph, btnMargs);
	
	
//setting style sheet ID's
	root.getStylesheets().addAll(this.getClass().getResource("style.css").toExternalForm());
	pieGraph.setId("graphButtons");
	lineGraph.setId("graphButtons");
	barGraph.setId("graphButtons");
	bubbleGraph.setId("graphButtons");
	
	}
	
}