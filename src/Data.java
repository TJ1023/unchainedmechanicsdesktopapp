import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//data type for each cyclist users

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {
	
	private data_points[] data_points;

	public data_points[] getData_points() {
		return data_points;
	}

	public void setData_points(data_points[] data_points) {
		this.data_points = data_points;
	}

}

class data_points{
	
	private String gender;
	private String apointment_dates;
	private int id;
	private String bikes;
	private int age;
	private String appointment_locations;
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBikes() {
		return bikes;
	}
	public void setBikes(String bikes) {
		this.bikes = bikes;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public String getApointment_dates() {
		return apointment_dates;
	}

	public void setApointment_dates(String apointment_dates) {
		this.apointment_dates = apointment_dates;
	}

	public String getAppointment_locations() {
		return appointment_locations;
	}

	public void setAppointment_locations(String appointment_locations) {
		this.appointment_locations = appointment_locations;
	}
	
	
}
