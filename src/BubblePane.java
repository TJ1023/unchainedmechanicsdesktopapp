
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.BubbleChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.*;

public class BubblePane extends BorderPane {

	private static final Object below18Giant = null;
	DataImporter data = new DataImporter();

	BubblePane() {

		// intialize the bike brands
		String giant = "Giant";
		String giant2 = "giant";
		String trek = "Trek";
		String trek2 = "trek";
		String bianchi = "Bianchi";
		String bianchi2 = "bianchi";
		String scott = "Scott";
		String scott2 = "scott";
		String gt = "GT";
		String gt2 = "gt";
		String merida = "Merida";
		String merida2 = "merida";
		String fuji = "Fuji";
		String fuji2 = "fuji";

		// chart title
		Label text = new Label("Bubble Chart");
		text.setAlignment(Pos.CENTER);
		text.setMaxSize(100, 50);
		// setCenter(text);
		setTop(text);
		setAlignment(text, Pos.CENTER);

		//(x,y,z) x = intial, y = axis max value, z = increments by
		final NumberAxis xAxis = new NumberAxis(0, 51, 1);
		final NumberAxis yAxis = new NumberAxis(0, 80, 5);

		final BubbleChart<Number, Number> blc = 
				new BubbleChart<Number, Number>(xAxis, yAxis);
		
		//give titles
		xAxis.setLabel("Days From Adding Test Data");
		yAxis.setLabel("Age");
		blc.setTitle("Age and Bike Brand Analytics");

		// bubble series
		XYChart.Series series1 = new XYChart.Series();
		series1.setName("Giant");
		XYChart.Series series2 = new XYChart.Series();
		series2.setName("Trek");
		XYChart.Series series3 = new XYChart.Series();
		series3.setName("Bianchi");
		XYChart.Series series4 = new XYChart.Series();
		series4.setName("GT");
		XYChart.Series series5 = new XYChart.Series();
		series5.setName("Scott");
		XYChart.Series series6 = new XYChart.Series();
		series6.setName("Merida");
		XYChart.Series series7 = new XYChart.Series();
		series7.setName("Fuji");
		XYChart.Series series8 = new XYChart.Series();
		series8.setName("Others");

		// initialize counter for age/trek
		int below18wTrek = 0;
		int f18to30wTrek = 0;
		int f31to50wTrek = 0;
		int f51to60wTrek = 0;
		int above60wTrek = 0;

		// initialize counter for age/giant
		int below18wGiant = 0;
		int f18to30wGiant = 0;
		int f31to50wGiant = 0;
		int f51to60wGiant = 0;
		int above60wGiant = 0;

		// initialize counter for age/bianchi
		int below18wBianchi = 0;
		int f18to30wBianchi = 0;
		int f31to50wBianchi = 0;
		int f51to60wBianchi = 0;
		int above60wBianchi = 0;

		// intialize counter for age/GT
		int below18wGT = 0;
		int f18to30wGT = 0;
		int f31to50wGT = 0;
		int f51to60wGT = 0;
		int above60wGT = 0;

		// intialize counter for age/Scott
		int below18wScott = 0;
		int f18to30wScott = 0;
		int f31to50wScott = 0;
		int f51to60wScott = 0;
		int above60wScott = 0;

		// intialize counter for age/Merida
		int below18wMerida = 0;
		int f18to30wMerida = 0;
		int f31to50wMerida = 0;
		int f51to60wMerida = 0;
		int above60wMerida = 0;

		// intialize counter for age/Fuji
		int below18wFuji = 0;
		int f18to30wFuji = 0;
		int f31to50wFuji = 0;
		int f51to60wFuji = 0;
		int above60wFuji = 0;

		// intialize counter for age/Other brands
		int below18wOthers = 0;
		int f18to30wOthers = 0;
		int f31to50wOthers = 0;
		int f51to60wOthers = 0;
		int above60wOthers = 0;

		// search and categorize data in respective bubbles
		for (int i = 0; i < data.getData().getData_points().length; i++) {

			int age = data.getData().getData_points()[i].getAge();

			String bike = data.getData().getData_points()[i].getBikes();

			if (bike.length() != 0) {

				//// giant case
				if ((bike.contains(giant) || bike.contains(giant2)) 
						&& (age < 18)) {
					below18wGiant++;
				}else if ((bike.contains(giant) || bike.contains(giant2))
						&& (age >= 18 && age <= 30)) {
					f18to30wGiant++;
				}else if ((bike.contains(giant) || bike.contains(giant2)) 
						&& (age >= 31 && age <= 50)) {
					f31to50wGiant++;
				}else if ((bike.contains(giant) || bike.contains(giant2)) 
						&& (age >= 51 && age <= 60)) {
					f51to60wGiant++;
				}else if ((bike.contains(giant) || bike.contains(giant2)) 
						&& (age > 60)) {
					above60wGiant++;
				}

				// trek case
				else if ((bike.contains(trek) || bike.contains(trek2)) 
						&& (age < 18)) {
					below18wTrek++;
				}else if ((bike.contains(trek) || bike.contains(trek2)) 
						&& (age >= 18 && age <= 30)) {
					f18to30wTrek++;
				}else if ((bike.contains(trek) || bike.contains(trek2)) 
						&& (age >= 31 && age <= 50)) {
					f31to50wTrek++;
				}else if ((bike.contains(trek) || bike.contains(trek2)) 
						&& (age >= 51 && age <= 60)) {
					f51to60wTrek++;
				}else if ((bike.contains(trek) || bike.contains(trek2))
						&& (age > 60)) {
					above60wTrek++;
				}

				// Bianchi case
				else if ((bike.contains(bianchi) || bike.contains(bianchi2)) 
						&& (age < 18)) {
					below18wBianchi++;
				}else if ((bike.contains(bianchi) || bike.contains(bianchi2)) 
						&& (age >= 18 && age <= 30)) {
					f18to30wBianchi++;
				}else if ((bike.contains(bianchi) || bike.contains(bianchi2)) 
						&& (age >= 31 && age <= 50)) {
					f31to50wBianchi++;
				}else if ((bike.contains(bianchi) || bike.contains(bianchi2))
						&& (age >= 51 && age <= 60)) {
					f51to60wBianchi++;
				}else if ((bike.contains(bianchi) || bike.contains(bianchi2)) 
						&& (age > 60)) {
					above60wBianchi++;
				}

				// GT case
				else if ((bike.contains(gt) || bike.contains(gt2)) 
						&& (age < 18)) {
					below18wGT++;
				}else if ((bike.contains(gt) || bike.contains(gt2)) 
						&& (age >= 18 && age <= 30)) {
					f18to30wGT++;
				}else if ((bike.contains(gt) || bike.contains(gt2))
						&& (age >= 31 && age <= 50)) {
					f31to50wGT++;
				}else if ((bike.contains(gt) || bike.contains(gt2))
						&& (age >= 51 && age <= 60)) {
					f51to60wGT++;
				}else if ((bike.contains(gt) || bike.contains(gt2)) 
						&& (age > 60)) {
					above60wGT++;
				}

				// Scott case
				else if ((bike.contains(scott) || bike.contains(scott2)) 
						&& (age < 18)) {
					below18wScott++;
				}else if ((bike.contains(scott) || bike.contains(scott2))
						&& (age >= 18 && age <= 30)) {
					f18to30wScott++;
				}else if ((bike.contains(scott) || bike.contains(scott2)) 
						&& (age >= 31 && age <= 50)) {
					f31to50wScott++;
				}else if ((bike.contains(scott) || bike.contains(scott2))
						&& (age >= 51 && age <= 60)) {
					f51to60wScott++;
				}else if ((bike.contains(scott) || bike.contains(scott2))
						&& (age > 60)) {
					above60wScott++;
				}

				// Merida case
				else if ((bike.contains(merida) || bike.contains(merida2)) 
						&& (age < 18)) {
					below18wMerida++;
				}else if ((bike.contains(merida) || bike.contains(merida2))
						&& (age >= 18 && age <= 30)) {
					f18to30wMerida++;
				}else if ((bike.contains(merida) || bike.contains(merida2))
						&& (age >= 31 && age <= 50)) {
					f31to50wMerida++;
				}else if ((bike.contains(merida) || bike.contains(merida2))
						&& (age >= 51 && age <= 60)) {
					f51to60wMerida++;
				}else if ((bike.contains(merida) || bike.contains(merida2)) 
						&& (age > 60)) {
					above60wMerida++;
				}

				// Fuji case
				else if ((bike.contains(fuji) || bike.contains(fuji2)) 
						&& (age < 18)) {
					below18wFuji++;
				}else if ((bike.contains(fuji) || bike.contains(fuji2)) 
						&& (age >= 18 && age <= 30)) {
					f18to30wFuji++;
				}else if ((bike.contains(fuji) || bike.contains(fuji2))
						&& (age >= 31 && age <= 50)) {
					f31to50wFuji++;
				}else if ((bike.contains(fuji) || bike.contains(fuji2)) 
						&& (age >= 51 && age <= 60)) {
					f51to60wFuji++;
				}else if ((bike.contains(fuji) || bike.contains(fuji2)) 
						&& (age > 60)) {
					above60wFuji++;
				}
				
				// Other brands case
				else if ((!bike.contains(giant) || !bike.contains(trek) 
						|| !bike.contains(bianchi)
						|| !bike.contains(gt) || !bike.contains(scott) 
						|| !bike.contains(merida)
						|| !bike.contains(fuji)) && (age < 18))
					below18wOthers++;
				
				else if ((!bike.contains(giant) || !bike.contains(trek) 
						|| !bike.contains(bianchi) || !bike.contains(gt)
						|| !bike.contains(scott) || !bike.contains(merida) 
						|| !bike.contains(fuji))
						&& (age >= 18 && age <= 30))
					f18to30wOthers++;
				
				else if ((!bike.contains(giant) || !bike.contains(trek) 
						|| !bike.contains(bianchi) || !bike.contains(gt)
						|| !bike.contains(scott) || !bike.contains(merida) 
						|| !bike.contains(fuji))
						&& (age >= 31 && age <= 50))
					f31to50wOthers++;
				
				else if ((!bike.contains(giant) || !bike.contains(trek)
						|| !bike.contains(bianchi) || !bike.contains(gt)
						|| !bike.contains(scott) || !bike.contains(merida)
						|| !bike.contains(fuji))
						&& (age >= 51 && age <= 60))
					f51to60wOthers++;
				
				else if ((!bike.contains(giant) || !bike.contains(trek)
						|| !bike.contains(bianchi) || !bike.contains(gt)
						|| !bike.contains(scott) || !bike.contains(merida)
						|| !bike.contains(fuji)) && (age > 60))
					above60wOthers++;

			}

		}

		// create Giant bubbles
		series1.getData().add(new XYChart.Data(1, 24, f18to30wGiant));
		series1.getData().add(new XYChart.Data(1, 18, below18wGiant));
		series1.getData().add(new XYChart.Data(1, 40, f31to50wGiant));
		series1.getData().add(new XYChart.Data(1, 55, f51to60wGiant));
		series1.getData().add(new XYChart.Data(1, 70, above60wGiant));

		// create Trek bubbles
		series2.getData().add(new XYChart.Data(5, 18, below18wTrek));
		series2.getData().add(new XYChart.Data(5, 24, f18to30wTrek));
		series2.getData().add(new XYChart.Data(5, 40, f31to50wTrek));
		series2.getData().add(new XYChart.Data(5, 55, f51to60wTrek));
		series2.getData().add(new XYChart.Data(5, 70, above60wTrek));

		// create Bianchi bubbles
		series3.getData().add(new XYChart.Data(10, 18, below18wBianchi));
		series3.getData().add(new XYChart.Data(10, 24, f18to30wBianchi));
		series3.getData().add(new XYChart.Data(10, 40, f31to50wBianchi));
		series3.getData().add(new XYChart.Data(10, 55, f51to60wBianchi));
		series3.getData().add(new XYChart.Data(10, 70, above60wBianchi));

		// create GT bubbles
		series4.getData().add(new XYChart.Data(15, 18, below18wGT));
		series4.getData().add(new XYChart.Data(15, 24, f18to30wGT));
		series4.getData().add(new XYChart.Data(15, 40, f31to50wGT));
		series4.getData().add(new XYChart.Data(15, 55, f51to60wGT));
		series4.getData().add(new XYChart.Data(15, 70, above60wGT));

		// create Scott bubbles
		series5.getData().add(new XYChart.Data(20, 18, below18wScott));
		series5.getData().add(new XYChart.Data(20, 24, f18to30wScott));
		series5.getData().add(new XYChart.Data(20, 40, f31to50wScott));
		series5.getData().add(new XYChart.Data(20, 55, f51to60wScott));
		series5.getData().add(new XYChart.Data(20, 70, above60wScott));

		// create Merida bubbles
		series6.getData().add(new XYChart.Data(25, 18, below18wMerida));
		series6.getData().add(new XYChart.Data(25, 24, f18to30wMerida));
		series6.getData().add(new XYChart.Data(25, 40, f31to50wMerida));
		series6.getData().add(new XYChart.Data(25, 55, f51to60wMerida));
		series6.getData().add(new XYChart.Data(25, 70, above60wMerida));

		// create Fuji bubbles
		series7.getData().add(new XYChart.Data(30, 18, below18wFuji));
		series7.getData().add(new XYChart.Data(30, 24, f18to30wFuji));
		series7.getData().add(new XYChart.Data(30, 40, f31to50wFuji));
		series7.getData().add(new XYChart.Data(30, 55, f51to60wFuji));
		series7.getData().add(new XYChart.Data(30, 70, above60wFuji));

		// create Others bubbles
		series8.getData().add(new XYChart.Data(35, 18, below18wOthers));
		series8.getData().add(new XYChart.Data(35, 24, f18to30wOthers));
		series8.getData().add(new XYChart.Data(35, 40, f31to50wOthers));
		series8.getData().add(new XYChart.Data(35, 55, f51to60wOthers));
		series8.getData().add(new XYChart.Data(35, 70, above60wOthers));


		// add all the bubble series
		blc.getData().addAll(series1, series2, series3, series4, series5, 
				series6, series7, series8);

		getStylesheets().addAll(this.getClass().getResource("style.css")
				.toExternalForm());
		setId("graphButtons");

		setCenter(blc);
	}

}
