
public class UsersCount {
	
	DataImporter data = new DataImporter();

	//the total cyclists 
	private int totalUsers;
	
	public UsersCount() {
		
		setTotalUsers(0);
		
		//count all the cyclists
		for(int i = 0; i < data.getData().getData_points().length; i++) {
			
			if(data.getData().getData_points()[i] != null)
				setTotalUsers(getTotalUsers() + 1);
		}
		
		//System.out.println("Total Cyclists: "+totalUsers);
		
	}

	public int getTotalUsers() {
		return totalUsers;
	}

	public void setTotalUsers(int totalUsers) {
		this.totalUsers = totalUsers;
	}
	
	
	
	
}
