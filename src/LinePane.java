
import javafx.scene.layout.BorderPane;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.chart.CategoryAxis;

public class LinePane extends BorderPane{
	
	DataImporter importer = new DataImporter();
	
	public LinePane(){
	
		//title of chart
		Label text = new Label("Line Chart");
		text.setAlignment(Pos.CENTER);
		text.setMaxSize(100, 50);
		//setCenter(text);
		setTop(text);
		setAlignment(text, Pos.CENTER);   
		//intialize x and y axis (month, number of appointments)
		final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Appointments");
        xAxis.setLabel("Month");
        final LineChart<String,Number> lineChart = 
                new LineChart<String,Number>(xAxis,yAxis);
		
        //title of the graph
		lineChart.setTitle("Bike Repairs Monitoring Test Data");
		
		//create data line 1, 2017 test data
	    XYChart.Series series1 = new XYChart.Series<>();
		series1.setName("Bike Repais 2017");
		series1.getData().add(new XYChart.Data("Jan", 6));
	    series1.getData().add(new XYChart.Data("Feb", 10));
	    series1.getData().add(new XYChart.Data("Mar", 12));
	    series1.getData().add(new XYChart.Data("Apr", 21));
	    series1.getData().add(new XYChart.Data("May", 26));
	    series1.getData().add(new XYChart.Data("Jun", 21));
	    series1.getData().add(new XYChart.Data("Jul", 16));
	    series1.getData().add(new XYChart.Data("Aug", 24));
	    series1.getData().add(new XYChart.Data("Sep", 31));
	    series1.getData().add(new XYChart.Data("Oct", 35));
	    series1.getData().add(new XYChart.Data("Nov", 22));
	    series1.getData().add(new XYChart.Data("Dec", 36));
	    
	    //create data line 2, 2018 test data
		XYChart.Series series2 = new XYChart.Series<>();
		series2.setName("Bike Repairs 2018");
		series2.getData().add(new XYChart.Data("Jan", 23));
	    series2.getData().add(new XYChart.Data("Feb", 14));
	    series2.getData().add(new XYChart.Data("Mar", 15));
	    series2.getData().add(new XYChart.Data("Apr", 24));
	    series2.getData().add(new XYChart.Data("May", 34));
	    series2.getData().add(new XYChart.Data("Jun", 36));
	    series2.getData().add(new XYChart.Data("Jul", 22));
	    series2.getData().add(new XYChart.Data("Aug", 45));
	    series2.getData().add(new XYChart.Data("Sep", 43));
	    series2.getData().add(new XYChart.Data("Oct", 17));
	    series2.getData().add(new XYChart.Data("Nov", 29));
	    series2.getData().add(new XYChart.Data("Dec", 25));
	  
	    //add the series data to the chart
	    lineChart.getData().addAll(series1,series2);
	    
	    getStylesheets().addAll(this.getClass().getResource("style.css")
	    		.toExternalForm());
	 	setId("graphButtons");
	    
	    setCenter(lineChart);
	     
	}

}
