import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.chart.*;

class PiePane extends BorderPane{
	
	DataImporter data= new DataImporter();
	private final ObservableList<PieChart.Data> pieChartData = 
			FXCollections.observableArrayList();
	final PieChart chart;
   
	@SuppressWarnings({ "restriction", "static-access" })
	public PiePane(){
		
		//intialize counters for big brand bikes
		int GiantCount = 0;
		int TrekCount = 0;
		int BianchiCount = 0;
		int ScottCount = 0;
		int GTcount = 0;
		int MeridaCount = 0;
		int FujiCount = 0;
		int OthersCount = 0;
		int totalbikes = 0;
		
		//go through and categorize data of bikes popularity
		for(int i = 0; i < data.getData().getData_points().length; i++) {
			
			String giant = "Giant";
			String giant2 = "giant";
			String trek = "Trek";
			String trek2 = "trek";
			String bianchi = "Bianchi";
			String bianchi2 = "bianchi";
			String scott = "Scott";
			String scott2 = "scott";
			String gt = "GT";
			String gt2 = "gt";
			String merida = "Merida";
			String merida2 = "merida";
			String fuji = "Fuji";
			String fuji2 = "fuji";
			
			String tempVar = data.getData().getData_points()[i].getBikes();
			 
			if(tempVar.length() != 0) {
				
				if(tempVar.contains(giant) || tempVar.contains(giant2)) {
					GiantCount++;
				}else if(tempVar.contains(trek) || trek.contains(trek2)) {
					TrekCount++;
				}else if(tempVar.contains(bianchi) || tempVar.contains(bianchi2)) {
					BianchiCount++;
				}else if(tempVar.contains(scott) || tempVar.contains(scott2)) {
					ScottCount++;
				}else if(tempVar.contains(gt) || tempVar.contains(gt2)) {
					GTcount++;
				}else if(tempVar.contains(merida) || tempVar.contains(merida2)) {
					MeridaCount++;
				}else if(tempVar.contains(fuji) || tempVar.contains(fuji2)) {
					FujiCount++;
				}else 
					OthersCount++;	
				
				totalbikes ++;
			}
		}
		
		System.out.println("Giant: " + GiantCount);
		System.out.println("Trek: " + TrekCount);
		System.out.println("Bianchi: " + BianchiCount);
		System.out.println("GT: " + GTcount);
		System.out.println("Scott: " + ScottCount);
		System.out.println("Merida: " + MeridaCount);
		System.out.println("Fuji: " + FujiCount);
		System.out.println("Others: " + OthersCount);
		
		//create all components in the constructor
		Label text = new Label("Pie Chart");
		text.setAlignment(Pos.CENTER);
		text.setMaxSize(100, 50);
		//setCenter(text);
		setTop(text);
		setAlignment(text, Pos.CENTER);
		//text.setEditable(false);

		//add data to the chart
		pieChartData.addAll(
				
				new PieChart.Data("Giant"+" "+calculatePercentage(GiantCount,
						totalbikes)+"%", GiantCount),
				new PieChart.Data("Bianchi"+" "+calculatePercentage(BianchiCount,
						totalbikes)+"%", BianchiCount),
				new PieChart.Data("Trek"+" "+calculatePercentage(TrekCount,
						totalbikes)+"%", TrekCount),
				new PieChart.Data("Scott"+" "+calculatePercentage(ScottCount,
						totalbikes)+"%", ScottCount),
				new PieChart.Data("GT"+" "+calculatePercentage(GTcount,
						totalbikes)+"%", GTcount),
				new PieChart.Data("Merida"+" "+calculatePercentage(MeridaCount,
						totalbikes)+"%", MeridaCount),
				new PieChart.Data("Fuji"+" "+calculatePercentage(FujiCount,
						totalbikes)+"%", FujiCount), 
				new PieChart.Data("Others"+" "+calculatePercentage(OthersCount,
						totalbikes)+"%", OthersCount));
	
		chart = new PieChart(pieChartData);
		chart.setTitle("Bike Brands Popularity");
		 
		   	        
	    getStylesheets().addAll(this.getClass().getResource("style.css")
	    		.toExternalForm());
	    setId("graphButtons");
	        
		setCenter(chart);

	}
	
	//do math (find percentage of the individual bike brand)
	public double calculatePercentage(double value, int totalbikes) {
		
		double percent = ((value/totalbikes)*100);
		percent = Math.round(percent*100);
		percent = percent/100;
		
		return percent;
	}

}