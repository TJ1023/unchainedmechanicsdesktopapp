import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.stage.*;

public class GUI extends Application{
	
	public static Stage window;
	public static Scene statScene;
	static double windowHeight = 720.0, windowWidth = 1025.0;
	ImageView on = new ImageView("greenStatus.png");
	ImageView off = new ImageView("redStatus.png");
	private HBox status = new HBox();
	int clientCount;
	String cCount = "---";
	Label userCount = new Label("Client Count:     " + cCount);
	Timer t = new Timer();
	
	//method called every time the clock cycles
	private void setServerStatus(boolean check){
		System.out.println("aghjkasdfkjghasdfkhjgs");
		if(check && status.getChildren().contains(on)){
			return;
		}else if(!check && status.getChildren().contains(off)){
			return;
		}else if(check){
			status.getChildren().remove(off);
			status.getChildren().add(on);
		}else if(!check){
			status.getChildren().remove(on);
			status.getChildren().add(off);
		}
	}
	@Override
	public void start(Stage primaryStage) throws Exception {

//insets used for magins in objects
		Insets btnMargs = new Insets(5, 5, 5, 5);
		Insets btnMargs2 = new Insets(5, 5, 10, 5);
		Insets btnMargs3 = new Insets(5, 5, 50, 5);
		Insets otherMargs = new Insets(5, 5, 350, 0);
		Insets logMargs = new Insets(5, 5, 200, 0);
		Insets fieldMargs = new Insets(0, 50, 0, 50);
		
//first pane before entering the app
		BorderPane homePane = new BorderPane();

//root is the base pane which we are adding other panes too
		BorderPane root = new BorderPane();
		Scene loginScene = new Scene(root, windowWidth, windowHeight);
		
//pane contains BG image for first scene
		BorderPane bgPane = new BorderPane();
		ImageView img = new ImageView("Unchained Menu.png");
		img.fitWidthProperty().bind(primaryStage.widthProperty());
		bgPane.setCenter(img);
		
//stack pane contains homePane and Background image
		StackPane login = new StackPane();
		login.getChildren().addAll(bgPane, homePane);
		root.setCenter(login);
		
//creates the "enter" button and assigns functionality to it
		VBox logMenu = new VBox(5);
		logMenu.setMaxSize(400, 200);
		logMenu.setAlignment(Pos.CENTER);
		Button signin = new Button("Access Data");
		//setting stylesheet ID and position on screen
		signin.setId("enterButton");
		signin.setTranslateX(155);
		signin.setTranslateY(90);
		//button pressed functionality
		signin.setOnMousePressed(e -> {
			signin.setId("enterButtonPressed");
			signin.setText("Loading. . .");
		});
		signin.setOnMouseReleased(e -> {
			signin.setId("enterButton");
			signin.setText("Access Data");
			primaryStage.setScene(new GraphScene(new BorderPane(), 
					windowWidth, windowHeight));
		});
		
//adds the enter button to the panel and sets dimensions
		logMenu.getChildren().addAll(signin);
		signin.setMinWidth(300);
		signin.setMinHeight(150);
		logMenu.setMargin(signin, btnMargs2);
		//sets position and location of logMenu in homePane
		homePane.setBottom(logMenu);
		homePane.setMargin(logMenu, logMargs);
		homePane.setAlignment(logMenu, Pos.CENTER);
		
//indicates location of the stylesheet for the given scene
		loginScene.getStylesheets().addAll(this.getClass()
				.getResource("style.css").toExternalForm());
//sideBar panel displays server status and client info
		VBox sideBar = new VBox();
		//sets size and location on on and off nodes
		on.setFitHeight(16);
		on.setFitWidth(16);
		on.setTranslateY(15);
		on.setTranslateX(18);
		
		off.setFitHeight(16);
		off.setFitWidth(16);
		off.setTranslateY(15);
		off.setTranslateX(18);
		
		//sets the location of the sideBar panel
		sideBar.setTranslateX(10);
		sideBar.setTranslateY(370);
		
		//
		Label server = new Label("Server Status  ");
		server.setId("serverStatus");
		userCount.setId("serverStatus");
		status.getChildren().addAll(server, off);
		sideBar.getChildren().addAll(status, userCount);
		homePane.setLeft(sideBar);
		
		
		/////////////Total Cyclists/////////////
		//int totalCyclists = CyclistsCount.getTotalUsers();
		
		//System.out.println("Total Cyclists: " + totalCyclists);
		
		
		/////////////check for server connection/////////////
		t.schedule(new TimerTask() {
		    @Override
		    public void run(){
		    	/* method called to use javaFX thread to access methods
		    	 * within the timer thread */
		    	Platform.runLater(new Runnable() {
	                @Override
	                public void run() {
	                	try{
	                		UsersCount CyclistsCount = new UsersCount();
	            			clientCount = CyclistsCount.getTotalUsers();
	                		DataImporter test = new DataImporter();
	                		setServerStatus(test.isServerStatus());
	                	}catch(Exception e){
	                		System.out.println("testakhsfdgkjhgasdf");
	                		setServerStatus(false);
	                		clientCount = -1;
	                	}
	                	if(clientCount > -1){
	            			cCount = Integer.toString(clientCount);
	            			userCount.setText("Client Count:     " + cCount);
	            		}else{
	            			cCount = "---";
	            			userCount.setText("Client Count:     " + cCount);
	            		}
	                }
	            });
		      
		    }
		},0, 5000);
		//sets the primary stage
		primaryStage.setScene(loginScene);
		primaryStage.setResizable(false);
		primaryStage.show();
		window = primaryStage;
		statScene = loginScene;	
	}
	//called to stop the timer when the window is closed
	@Override
	public void stop(){
		t.cancel();
	}
	public static void main(String args[]){
		launch(args);
	}	
}
