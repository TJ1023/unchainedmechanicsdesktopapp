import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import java.sql.*;
import java.math.*;
import java.net.URL;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataImporter {
	
	Data data;
	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	String url;
	static String uN;
	static String pW;
	
	//this will check for server status in the homepage(GUI.java)
	boolean serverStatus;
	
	public DataImporter() {
		
	//access data by going to the URL 
	//and use the Secret/Private key for authentication
		 try {
	            String secret_key = "1WMMgrzUkZ8CJz8OfMVmPqzrMxQqXe7yKQbk3a7VYhZ7"
	            		+ "MOsSs8guu7MeVtOmA0zDoWvfc4t56QsRnGtrfn75lhm7qoU9tmgL7y"
	            		+ "NGRin6xvMGFUjO4CLT24qGf9rpYzXTG3qSqMvDJdJQIhhlEUVC3kON"
	            		+ "6QnO3ZWIbZJtllf10rb5UKX6OuMEHRSAY8aEV7OgaHdhb8r28LkSLj"
	            		+ "zQmjRZV1W3PCF0BDqiLkPaVmt4jp5MFkpdIYTeC9J5VkVxLd0FKeOo"
	            		+ "BtXaFDQ7XXITKYC6z3dM707AUDkDuiheRvDJhZxshvH2mbZGXdnGlY"
	            		+ "36YHFoMj4Pg473RoYn2MKxgmReNp5zITsMq1Nkucf6pY0yJEYlhhRF"
	            		+ "q6UppNLg3wzItu3RMAlK8XgkH0tcCaluLfhsRXd57iADCi9Y44E7Td"
	            		+ "29ETdGcBSJRSTSHaWZzkzHnXX1aI3co3lWWAflX4TX7TcVg0RcGVYe"
	            		+ "OPJKz4Fc2m5mot5TrXNFeC6smfoc0YmszNQo";
	            String public_key =
	                    "OS5zU7Ta5Lzw6PrbKYJAWFBxc880nkotVpdxC64QcfZjL6m0WZXN2B53"
	                    + "qcE54A84uOXCmszTAwVcsXC5Xn0x6EUmlQm8CBhFVtZ9FANDaAow0W"
	                    + "93OJVhChcjAiN4EirZ2KGD2Ubirv50gtJRysgid55hA5x16TozmL6F"
	                    + "fBxB9sqZr35gwO23T1IwyszRYzGGiFSwcrUYHyXOuJoAEl4c5ZT2Vw"
	                    + "y22NiZYSy5nYCgoWj1Qa5sQXFskF7VZZdd4ES67dDh4MAgMseXsgJk"
	                    + "ZuxAghHp5DcsphxatnqDnzQ1bxwzMGhz7ATZWeD0oBjja5N0DyDYwt"
	                    + "TKZZRg0wEpBiw21zjyMBwYBWg08Tklwj1XqtdxloAgHkdTVyopIHCD"
	                    + "B0TFO9d6bV0pU6JrW4m0EoS2PCT7MJXa8QgpdRyCG2gllN50pTZexP"
	                    + "ELsA9acgfMup7Tu1FbxCUVWHNdRGaaq8wDs2aQzeh4fSsnPPu7wPdr"
	                    + "h2IGVXhDTZuENRM8vc2wvPHd";

	            URL src = new URL("http://ec2-18-218-165-245.us-east-2.compute"
	            		+ ".amazonaws.com/API/test/" + secret_key
	                    + public_key);
	            
		        //map data to the created java data type    
	            final ObjectMapper mapper = 
	            	    new ObjectMapper()
	            	        .configure(DeserializationFeature
	            	        		.FAIL_ON_UNKNOWN_PROPERTIES, false);
			 
	            data = mapper.readValue(src, Data.class);
	               	    		
	            setServerStatus(true);
		  }catch (Exception e) {
			  	setServerStatus(false);
	            e.printStackTrace();
	            
	      }		
		 
	}

	public boolean isServerStatus() {
		return serverStatus;
	}

	public void setServerStatus(boolean serverStatus) {
		this.serverStatus = serverStatus;
	}
	
}